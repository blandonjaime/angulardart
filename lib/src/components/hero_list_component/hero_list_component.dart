import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:angular_app/src/routes/route_paths.dart';

import 'package:angular_app/src/services/hero_service.dart';
import 'package:angular_app/src/hero.dart';

@Component(
  selector: 'my-heroes',
  templateUrl: 'hero_list_component.html',
  styleUrls: ['hero_list_component.css'],
  directives: [
    coreDirectives,
  ],
  providers: [ClassProvider(HeroService)],
  pipes: [commonPipes],
)
class HeroListComponent implements OnInit {
  final HeroService _heroService;
  final Router _router;

  HeroListComponent(this._heroService, this._router);

  var name = 'Angular';
  final String title = 'Tour of Heroes';
  List<Hero> heroes;
  Hero selected;

  void ngOnInit() {
    _getHeroes();
  }

  void onSelect(Hero hero) {
    print(hero);
    selected = hero;
  }

  Future<void> _getHeroes() async {
    //heroes = _heroService.getAll(); SINCRONO
    heroes = await _heroService.getAll();
  }

  String _heroUrl(int id) =>
      RoutePaths.hero.toUrl(parameters: {idParam: '$id'});

  Future<NavigationResult> gotoDetail() =>
      _router.navigate(_heroUrl(selected.id));

  Future<void> add(String name) async {
    name = name.trim();
    if (name.isEmpty) return null;
    heroes.add(await _heroService.create(name));
    selected = null;
  }

  Future<void> delete(Hero hero) async {
    await _heroService.delete(hero.id);
    heroes.remove(hero);
    if (selected == hero) selected = null;
  }
}
