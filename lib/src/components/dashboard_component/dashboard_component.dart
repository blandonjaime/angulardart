import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'package:angular_app/src/services/hero_service.dart';
import 'package:angular_app/src/hero.dart';

import 'package:angular_app/src/routes/route_paths.dart';

import 'package:angular_app/src/components/hero_search_component/hero_search_component.dart';

@Component(
  selector: 'my-dashboard',
  templateUrl: 'dashboard_component.html',
  styleUrls: ['dashboard_component.css'],
  directives: [coreDirectives,HeroSearchComponent,routerDirectives],
  //directives: [coreDirectives,routerDirectives],
)
class DashboardComponent implements OnInit {

  List<Hero> heroes;
  final HeroService _heroService;
  String heroUrl(int id) => RoutePaths.hero.toUrl(parameters: {idParam: '$id'});

  DashboardComponent(this._heroService);

  @override 
  void ngOnInit() async {

    heroes = (await _heroService.getAll()).skip(1).take(4).toList();

  }

}